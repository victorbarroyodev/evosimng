// 4 tick / sec
export const PLAY_SPEED = 250;

// 10 tick / sec
export const FAST_SPEED = 100;

// 40 tick / sec
export const VERY_FAST_SPEED = 25;
