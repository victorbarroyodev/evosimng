import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PainterService {
  painterTool: BehaviorSubject<'NONE'|'SEED'|'WATER'> = new BehaviorSubject<'NONE'|'SEED'|'WATER'>('NONE');
  
  changePaintenTool(tool:'NONE'|'SEED'|'WATER'){
    this.painterTool.next(tool);
  }

  get $painterToolObservable():Observable<'NONE'|'SEED'|'WATER'>{
    return this.painterTool.asObservable();
  }
}
