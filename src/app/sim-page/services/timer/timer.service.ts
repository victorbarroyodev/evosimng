import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { FAST_SPEED, PLAY_SPEED, VERY_FAST_SPEED } from 'src/app/conf/timer.conf';

@Injectable({
  providedIn: 'root'
})
export class TimerService {
  private tickSubject:Subject<void> = new Subject<void>();
  private intervalReference:NodeJS.Timer|undefined = undefined;
  private isPaused:boolean=false;
  private intervalSpeed = PLAY_SPEED;

  constructor() {
    this.runTheTime();
  }

  /**
   * Change de speed of de simulation
   * @param speed speed of de simulation
   */
  changeSpeed(speed:number){
    switch(speed){
      case 0:
        this.isPaused = true;
        break;
      case 1:
        this.isPaused = false;
        this.intervalSpeed = PLAY_SPEED;
        break;
      case 2:
        this.isPaused = false;
        this.intervalSpeed = FAST_SPEED;
        break;
      case 3:
        this.isPaused = false;
        this.intervalSpeed = VERY_FAST_SPEED;
        break;
      default:
        this.isPaused = false;
        this.intervalSpeed = PLAY_SPEED;
    }
    this.runTheTime();
  }

  /**
   * initialize or update de time
   */
  private runTheTime(){
    if (this.intervalReference){
      clearInterval(this.intervalReference);
    }
    if(!this.isPaused){
      this.intervalReference= setInterval(()=>{
        this.tickSubject.next();
      },this.intervalSpeed);  
    }
  }
  
  get $timeObservable():Observable<void>{
    return this.tickSubject.asObservable();
  }

}
