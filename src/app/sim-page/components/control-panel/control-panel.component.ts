import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { PainterService } from '../../services/painter/painter.service';
import { TimerService } from '../../services/timer/timer.service';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss']
})
export class ControlPanelComponent{
  subscriptions: Subscription[]=[];
  timeSelection:0|1|2|3 = 1;

  constructor(
    private timerService:TimerService,
    private painterService:PainterService
  ) { }

  /**
   * change de speed of the sim
   * @param speed speed of the sim
   */
  changeTimeSpeed(speed:0|1|2|3){
    this.timeSelection = speed;
    this.timerService.changeSpeed(this.timeSelection);
  }

  changeTool(tool:'NONE'|'SEED'|'WATER'){
    this.painterService.changePaintenTool(tool);
  }

}
