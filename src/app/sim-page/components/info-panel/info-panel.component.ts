import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Tile } from 'src/app/shared/models/tile.model';
import { ComunicationService } from 'src/app/shared/services/comunication/comunication.service';

@Component({
  selector: 'app-info-panel',
  templateUrl: './info-panel.component.html',
  styleUrls: ['./info-panel.component.scss']
})
export class InfoPanelComponent implements OnInit, OnDestroy {
  tile:Tile|undefined;
  subscriptions: Subscription[]=[];

  constructor(private comunitationService: ComunicationService) { }

  ngOnInit(): void {
    this.subscriptions.push(
      this.comunitationService.$clickedTileObservable.subscribe((tile)=>{
        this.tile=tile;
      })
    );  
  }
  
  ngOnDestroy(): void {
    this.subscriptions.forEach((e)=>{
      e.unsubscribe();
    })
  }

}
