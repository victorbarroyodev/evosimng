import { Component, OnInit } from '@angular/core';
import { WORLD_SIZE } from 'src/app/conf/world.conf';

@Component({
  selector: 'app-world-panel',
  templateUrl: './world-panel.component.html',
  styleUrls: ['./world-panel.component.scss']
})
export class WorldPanelComponent implements OnInit {

  gridSize: number = WORLD_SIZE;
  grid:string[][] = [];

  ngOnInit(): void {
    for(let i = 0; i< this.gridSize; i++){
      let numbersRow:string[] =[];
      for(let j = 0; j< this.gridSize; j++){
        numbersRow.push(i + '-' + j);
      }
      this.grid.push(numbersRow);
    }
  }

}
