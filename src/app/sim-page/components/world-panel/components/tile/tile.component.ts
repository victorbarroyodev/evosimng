import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { INITIAL_WORLD_FERT_LEVEL, WORLD_DEGRADATE_RATE } from 'src/app/conf/world.conf';
import { TimerService } from 'src/app/sim-page/services/timer/timer.service';
import { probCalc } from 'src/app/shared/utils/math.utils';
import { ComunicationService } from 'src/app/shared/services/comunication/comunication.service';
import { PlantSeed } from 'src/app/shared/models/plantSeed.model';
import { PlantFruit } from 'src/app/shared/models/plantFruit.model';
import { Plant } from 'src/app/shared/models/plant.model';
import { BASIC_PLANT_GENOMA } from 'src/app/conf/genoma.conf';
import { PainterService } from 'src/app/sim-page/services/painter/painter.service';

@Component({
  selector: 'app-tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss']
})
export class TileComponent implements OnInit {
  @Input() id: string | undefined;
  subscriptions: Subscription[]=[];
  fertLevel: number = INITIAL_WORLD_FERT_LEVEL;
  degradateProb:number = WORLD_DEGRADATE_RATE;
  tool:'NONE'|'SEED'|'WATER'='NONE'
  seedCharge=true;
  isAWaterTile:boolean=false;

  plants:Plant[]=[];
  seeds:PlantSeed[]=[];
  fruits:PlantFruit[]=[];

  constructor(
    private timerService:TimerService, 
    private comunitationService: ComunicationService,
    private painterService: PainterService
  ) { }

  ngOnInit(): void {
    this.subscriptions.push(
      this.timerService.$timeObservable.subscribe(()=>{
        this.onTimerTick();
      })
    ); 
    this.subscriptions.push(
      this.comunitationService.$newSeedobservable.subscribe((newSeed:PlantSeed)=>{
        if(newSeed.tileID=== this.id && this.seeds.length<2+this.fertLevel/75){
          this.seeds.push(newSeed);
        }
      })
    );
    this.subscriptions.push(
      this.comunitationService.$newfruitObservable.subscribe((newFruit:PlantFruit)=>{
        if(newFruit.tileID=== this.id){
          this.fruits.push(newFruit);
        }
      })
    );
    this.subscriptions.push(
      this.painterService.$painterToolObservable.subscribe((tool:'NONE'|'SEED'|'WATER')=>{
        this.tool=tool;
      })
    );
    this.subscriptions.push(
      this.comunitationService.$giveFertObservable.subscribe((givedFert)=>{
        if(givedFert.pos=== this.id){
          this.fertLevel+=givedFert.val;
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((e)=>{
      e.unsubscribe();
    })
  }

  /**
   * emit this tile data to be displayed on the aside info panel
   */
  emitClickedTile(){

    switch(this.tool){
      case 'NONE':
        this.comunitationService.nextclickedTileEvent({
          id: this.id,
          fertLVL: this.fertLevel
        });
        break;
      case 'SEED':
        let genoma = {...BASIC_PLANT_GENOMA};
        if(this.id)
          genoma.tileID =this.id;

        this.seeds.push(new PlantSeed(genoma));
        this.seedCharge = false;
        break;
      case 'WATER':
        this.isAWaterTile=!this.isAWaterTile;
    }
  }

  /**
   * This func is called in every tick of the sim
   */
  private onTimerTick(){
   this.degradateTerrain();
   this.feedPlants();
   this.tickPlants();
   this.tickFruits();
   this.tickSeeds();
  }

  /**
   * reduce the fert of the terrain over the time
   */
  private degradateTerrain(){
    if(probCalc(this.degradateProb)){
      if(this.isAWaterTile){
        this.fertLevel=2000;
      }
      if(this.id){
        let xMov:number = 0;
        let yMov:number = 0;
        if(probCalc(.5)){
            xMov= probCalc(.5)? -1: 1;
        }else{
            yMov= probCalc(.5)? -1: 1;
        }
        
        let x = parseInt(this.id.substring(0, this.id.indexOf('-')));
        let y = parseInt(this.id.substring(this.id.indexOf('-')+1));
        this.comunitationService.nextGiveFertEvent((x+xMov)+'-'+(y+yMov), this.fertLevel/20) ;
      }
      this.fertLevel -= (this.fertLevel/20);
      this.fertLevel -= (this.fertLevel/100);
    }
  }
  
  private feedPlants(){
    let nutrientsAvalibles = this.fertLevel/10 - Math.pow(this.fertLevel/100,1.6)-2;
    if (nutrientsAvalibles<.3){
      nutrientsAvalibles=.3;
    }
    this.plants.sort((a:Plant, b:Plant)=>{
      if(a.currentSize > b.currentSize){
        return -1;
      }else if(a.currentSize < b.currentSize){
        return 1;
      }else{
        return 0;
      }
    });

    this.plants.forEach(plant=>{
      nutrientsAvalibles=plant.obtainNutrients(nutrientsAvalibles);
    });
  }

  private tickPlants(){
    this.plants.forEach(plant=>{
      const nutrientsReturned = plant.tick();
      if (nutrientsReturned){
        this.fertLevel+= nutrientsReturned;
        this.plants.splice(this.plants.indexOf(plant) ,1);
      }
    });
  }

  private tickFruits(){
    this.fruits.forEach(fruit=>{
      const nutrientsReturned = fruit.tick(this.fertLevel);
      if (nutrientsReturned){
        this.fertLevel+= nutrientsReturned;
        this.fruits.splice(this.fruits.indexOf(fruit) ,1);
      }
    });
  }

  private tickSeeds(){
    this.seeds.forEach(seed=>{
      const plantGenoma = seed.tick();
      if(plantGenoma){
        if(this.plants.length<1+this.fertLevel/75 && !this.isAWaterTile)
          this.plants.push(new Plant(plantGenoma, this.comunitationService));
        this.seeds.splice(this.seeds.indexOf(seed) ,1);
      }
    });
  }
  
  get someFreshFruit():boolean{
    return this.fruits.some(f=>f.isFresh);
  }

  get someRottenFruit():boolean{
    return this.fruits.some(f=>!f.isFresh);
  }

}
