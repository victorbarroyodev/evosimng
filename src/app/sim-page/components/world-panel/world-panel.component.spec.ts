import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorldPanelComponent } from './world-panel.component';

describe('WorldPanelComponent', () => {
  let component: WorldPanelComponent;
  let fixture: ComponentFixture<WorldPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorldPanelComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WorldPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
