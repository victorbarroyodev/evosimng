import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimPageComponent } from './sim-page.component';
import { ControlPanelComponent } from './components/control-panel/control-panel.component';
import { InfoPanelComponent } from './components/info-panel/info-panel.component';
import { WorldPanelComponent } from './components/world-panel/world-panel.component';
import { TileComponent } from './components/world-panel/components/tile/tile.component';




@NgModule({
  declarations: [
    SimPageComponent,
    ControlPanelComponent,
    InfoPanelComponent,
    WorldPanelComponent,
    TileComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SimPageComponent
  ]
})
export class SimPageModule { }
