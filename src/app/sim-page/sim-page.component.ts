import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PainterService } from './services/painter/painter.service';

@Component({
  selector: 'app-sim-page',
  templateUrl: './sim-page.component.html',
  styleUrls: ['./sim-page.component.scss']
})
export class SimPageComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[]=[];
  cursorStyle:'NONE'|'SEED'|'WATER'='NONE'

  constructor(private painterServie:PainterService){}

  ngOnInit(): void {
    this.subscriptions.push(
      this.painterServie.$painterToolObservable.subscribe((tool:'NONE'|'SEED'|'WATER')=>{
        console.log(tool);
        
        this.cursorStyle=tool;
      })
    );     
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((e)=>{
      e.unsubscribe();
    })
  }
}
