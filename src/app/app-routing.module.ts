import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SimPageComponent } from './sim-page/sim-page.component';

const routes: Routes = [
  {path: '', component: SimPageComponent},
  {path: '**', component: SimPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
