import { probCalc } from "../utils/math.utils";
import { PlantGenoma } from "./plantGenoma.model";

export class PlantSeed{
    tileID:string;
    private germinationTimeLeft:number;
    private plantGenoma:PlantGenoma;

    constructor(plantGenoma:PlantGenoma){
        this.tileID = plantGenoma.tileID;
        this.germinationTimeLeft = plantGenoma.germinateTime;
        this.plantGenoma = plantGenoma; 
    }

    tick(): PlantGenoma|void{
        this.germinationTimeLeft--;
        if(this.germinationTimeLeft<=0){
            if(probCalc(this.plantGenoma.germinateRate)){
                return this.plantGenoma;
            }
        }
    }
}