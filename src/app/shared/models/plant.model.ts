import { ComunicationService } from "../services/comunication/comunication.service";
import { capacityMutation, probCalc, qualityMutation } from "../utils/math.utils";
import { PlantFruit } from "./plantFruit.model";
import { PlantGenoma } from "./plantGenoma.model";
import { PlantSeed } from "./plantSeed.model";

/**
 * Height ====> most important atribute of a plant
 * targetHealth = Height*10
 * maxNutrientsPerTick = Height*100
 * maxNutrients = Height*750
 * liveEnergyConsumption = Height*30      * (this.propagateRate * 20)
 * propagationEnergyConsumption = Height*500
 */

export class Plant{
    private tileID:string;

    //fundamental stat
    private maxComplexity:number=10;
    private genomaInestability=.05;

    //initial stats
    private targetHeight:number;
    private targetLiveExpectancy:number;
    private propagateRate:number;
    private hardness:number;
    private germinateTime:number;
    private germinateRate:number;
    private hasBark:boolean;
    private hasThorns:boolean;
    private hasPoison:boolean;
    private hasFruits:boolean;
    private hasDeepRoots:boolean;
    private hasSuperGrowUpSpeed:boolean;
    private hasVainas:boolean;
    private hasInmortalGen:boolean;
    private hasMortalGen:boolean;
    private hasGigantoGen:boolean;
    private hasMiniGen:boolean;
    private hasSporeGen:boolean;
    private hasFertilizerGen:boolean;
    private hasStoreGen:boolean;
    private hasComplexGen:boolean;
    private hasFrugalGen:boolean;
    private hasInestableGen:boolean;
    private hasStableGen:boolean;
    private hasInShadowsGrow:boolean;
    
    //derivated stats
    private maxGrowUpSpeed; 
    private atack:number = 0;
    private armor;
    private growRate: number;
    private complexity:number = 0;

    //dynamic stats
    private height:number = .01;
    private health:number = .1;
    private age:number = 0;
    private nutrients: number = 0;
    
    //dynamic derivated stats
    private get targetHealth():number{
        return this.height * 10;
    };
    private get maxNutrientsPerTick():number{
        return this.health * 10;
    };
    private get maxNutrients():number{
        return this.height * 750;
    };

    private get liveEnergyConsumption():number{
        return this.height * 600 * this.propagateRate;
    }

    private get propagationEnergyConsumption():number{
        return this.targetHeight * 500 / (this.germinateTime/100);
    }

    constructor(
        plantGenoma:PlantGenoma,
        private comunicationService: ComunicationService
    ){
        this.tileID=plantGenoma.tileID;
        this.targetHeight=plantGenoma.targetHeight < 0.02? 0.02:plantGenoma.targetHeight;
        this.targetLiveExpectancy=plantGenoma.targetLiveExpectancy;
        this.propagateRate=plantGenoma.propagateRate;
        this.hardness=plantGenoma.hardness;
        this.germinateTime = plantGenoma.germinateTime;
        this.germinateRate = plantGenoma.germinateRate;
        this.armor = this.hardness;
        this.maxGrowUpSpeed = 1.06 - this.hardness/100;

        this.hasBark=plantGenoma.hasBark;
        this.hasThorns=plantGenoma.hasThorns;
        this.hasPoison=plantGenoma.hasPoison;
        this.hasFruits=plantGenoma.hasFruits;
        this.hasDeepRoots=plantGenoma.hasDeepRoots;
        this.hasSuperGrowUpSpeed = plantGenoma.hasSuperGrowUpSpeed;
        this.hasVainas = plantGenoma.hasVainas;
        this.hasInmortalGen=plantGenoma.hasInmortalGen;
        this.hasMortalGen=plantGenoma.hasMortalGen;
        this.hasGigantoGen=plantGenoma.hasGigantoGen;
        this.hasMiniGen=plantGenoma.hasMiniGen;
        this.hasSporeGen=plantGenoma.hasSporeGen;
        this.hasFertilizerGen=plantGenoma.hasFertilizerGen;
        this.hasStoreGen=plantGenoma.hasStoreGen;
        this.hasComplexGen=plantGenoma.hasComplexGen;
        this.hasFrugalGen=plantGenoma.hasFrugalGen;
        this.hasInestableGen=plantGenoma.hasInestableGen;
        this.hasStableGen=plantGenoma.hasStableGen;
        this.hasInShadowsGrow=plantGenoma.hasInShadowsGrow;

        if(this.hasBark){
            this.armor*=2;
        }
        if(this.hasThorns){
            this.atack=5;
        }
        if(this.hasComplexGen){
            this.maxComplexity*=2.6;
        }
        if(this.hasInmortalGen){
            this.targetLiveExpectancy*=1.01;
        }
        if(this.hasMortalGen){
            this.targetLiveExpectancy*=.99;
        }
        if(this.hasGigantoGen){
            this.targetHeight*=1.01;
        }
        if(this.hasMiniGen){
            this.targetHeight*=.99;
        }
        
        let capacityMutations:boolean[]=[
            this.hasBark,
            this.hasThorns,
            this.hasPoison,
            this.hasFruits,
            this.hasDeepRoots,
            this.hasSuperGrowUpSpeed,
            this.hasVainas,
            this.hasInmortalGen,
            this.hasMortalGen,
            this.hasGigantoGen,
            this.hasMiniGen,
            this.hasSporeGen,
            this.hasFertilizerGen,
            this.hasStoreGen,
            this.hasComplexGen,
            this.hasFrugalGen,
            this.hasInestableGen,
            this.hasStableGen,
            this.hasInShadowsGrow
        ];

        capacityMutations.forEach(mut => {
            if(mut)
                this.complexity++;
        });

        this.growRate= 1-(this.complexity/this.maxComplexity);
    }

    /**
     * Obtain the nutrients of te nutrients avalibles of the soil
     * @param nutrientsAvalibles 
     * @returns 
     */
    obtainNutrients(nutrientsAvalibles:number):number{
        // init vals
        let maxNuTick= this.maxNutrientsPerTick;

        // mutations
        if(this.hasDeepRoots){
            maxNuTick*=1.5;
        }

        let nutrientsLeft = nutrientsAvalibles;
        if(nutrientsAvalibles>maxNuTick){
            this.nutrients+=maxNuTick;
            nutrientsLeft = nutrientsAvalibles-maxNuTick;
        }else{
            this.nutrients+= nutrientsAvalibles;
            nutrientsLeft=0;
        }    
        
        return nutrientsLeft;
    }

    /**
     * called every tick
     * @returns if return is !=0, then the plant dies
     */
    tick():number{
        this.consumeEnergyToLive();
        this.consumeEnergyToRegenerate();
        this.consumeEnergyToGrowUp();
        this.consumeEnergyToPropagate();
        this.consumeEnergyToGiveFruit();
        this.storeEnergy();
        this.getOlder();
        return this.checkIfStillAlive();

    }

    /**
     * Spent energy to keep living
     */
    private consumeEnergyToLive():void{
        // init vals
        let consum = this.liveEnergyConsumption;

        // mutations
        if(this.hasFrugalGen){
            consum*=.7
        }
        if(this.hasInShadowsGrow){
            if(this.height<this.targetHeight){
                consum=0;
            }else{
                consum*=3;
            }
        }

        if (this.nutrients>= consum){
            this.nutrients-= consum
        }else{
            this.health -= (consum-this.nutrients);
            this.nutrients = 0;
        }
    }

    /**
     * Spent energy to keep the live to max
     */
    private consumeEnergyToRegenerate():void{
        // init vals
        let maxHealth = this.targetHealth;

        // mutations


        if(this.health<maxHealth){
            if( this.nutrients>= (maxHealth-this.health)){
                this.nutrients -= maxHealth-this.health;
                this.health = maxHealth;
            }else{
                this.health+=this.nutrients;
                this.nutrients=0;
            }
        }
    }

    /**
     * Spent energy to grow up
     */
    private consumeEnergyToGrowUp():void{
        // init vals
        let maxHealth = this.targetHealth;
        let maxGrow = this.maxGrowUpSpeed;

        // mutations
        if(this.hasSuperGrowUpSpeed){
            maxGrow+=.25
        }

        if (this.height<this.targetHeight){
            if(probCalc(this.growRate)){
                this.height= (this.height * (Math.pow(1.01,this.nutrients)>maxGrow? maxGrow: Math.pow(1.01,this.nutrients))) ;
                this.health = maxHealth;
            }
            this.nutrients=0;
        }
    }

    /**
     * Spent energy to keep the reproduce
     */
    private consumeEnergyToPropagate():void{
        // init vals
        let propRate = this.propagateRate;
        let propConsum = this.propagationEnergyConsumption

        // mutations
        if(this.hasVainas){
            propRate*=.6;
            propConsum*=1.3;
        }

        if(probCalc(propRate) && this.nutrients>= propConsum) {
            this.nutrients-=propConsum;
            this.comunicationService.nextnewSeedEvent(new PlantSeed(this.mutate()));
            // mutations
            if(this.hasVainas){
                this.comunicationService.nextnewSeedEvent(new PlantSeed(this.mutate()));
                this.comunicationService.nextnewSeedEvent(new PlantSeed(this.mutate()));
            }
        }
    }

    /**
     * Spent energy to generate a fruit
     */
    private consumeEnergyToGiveFruit():void{
        if(this.hasFruits && this.nutrients>=this.maxNutrients){
            this.nutrients *=.25;
            this.comunicationService.nextnewFruitEvent(new PlantFruit(this.tileID, this.nutrients*3));
        }
    }

    /**
     * Store the excess energy
     */
    private storeEnergy():void{
        // init vals
        let maxNu = this.maxNutrients;

        // mutations
        if(this.hasSporeGen){
            maxNu*=10;
        }

        if(this.nutrients >= maxNu){
            this.nutrients = maxNu;
        }
    }

    /**
     * Count the lived cicles, and dies if it's too old
     */
    private getOlder():void{
        // init vals
        let maxAge = this.targetLiveExpectancy

        // mutations


        this.age++;
        if(this.age >= maxAge){
            this.health=0;
        }
    }

    /**
     * Check if the plant survives this cicle
     * @returns if return is !=0, then the plant dies
     */
    private checkIfStillAlive():number{
        if(this.health<=0){
            if(this.health<0)
                this.health=0;
            console.log(
                "------------ \n"+
                "ALTURA: " + this.height.toFixed(2) + "/" + this.targetHeight.toFixed(2)+"\n"+
                "VIDA: " + this.health.toFixed(2) +"/" + this.targetHealth.toFixed(2)+"\n"+
                "EDAD: " + this.age.toFixed(2) +"/" + this.targetLiveExpectancy.toFixed(2)+"\n"+
                "ENERGIA: "+ this.nutrients.toFixed(2)+"/" + this.maxNutrients.toFixed(2)+"\n"+
                "PROPAGATE COST: "+ this.propagationEnergyConsumption.toFixed(2)+"\n"+
                "DUREZA: " + this.hardness.toFixed(2)+"\n"+
                "REPRODUTION: " + this.propagateRate.toFixed(2)+"\n"+
                "BASAL: " + this.liveEnergyConsumption.toFixed(2)+"\n"+
                "COMPLEJIDAD: " + this.complexity +"\n\n"
            );
            // init vals
            let returnedEnergy = (this.targetHealth- .1)*4+0.001

            // mutations
            if(this.hasFertilizerGen){
                returnedEnergy*=4;
            }
            
            return returnedEnergy;
        }
        return 0;
    }

    /**
     * Generate a genoma similar to this plant genoma, but with some mutations
     * @returns a genoma similar to this plant genoma, but with some mutations
     */
    private mutate():PlantGenoma{
        // init vals
        let genomaInestability= this.genomaInestability;
        let newHabProb= this.genomaInestability*.02;

        //mutations
        if(this.hasInestableGen){
            genomaInestability*=2.5;
            newHabProb*=10;
        }
        if(this.hasStableGen){
            genomaInestability*=.4;
            newHabProb*=.1;
        }

        return {
            tileID: this.determinatePropagationDirection(this.tileID),
            targetHeight: qualityMutation(this.targetHeight, genomaInestability),
            targetLiveExpectancy: qualityMutation(this.targetLiveExpectancy, genomaInestability),
            propagateRate: qualityMutation(this.propagateRate, genomaInestability),
            hardness: qualityMutation(this.hardness, genomaInestability),
            germinateTime: qualityMutation(this.germinateTime, genomaInestability),
            germinateRate: qualityMutation(this.germinateRate, genomaInestability),
            hasBark: capacityMutation(this.hasBark, newHabProb),
            hasThorns: capacityMutation(this.hasThorns, newHabProb),
            hasPoison: capacityMutation(this.hasPoison, newHabProb),
            hasFruits: capacityMutation(this.hasFruits, newHabProb),
            hasDeepRoots: capacityMutation(this.hasDeepRoots, newHabProb),
            hasSuperGrowUpSpeed: capacityMutation(this.hasSuperGrowUpSpeed, newHabProb),
            hasVainas: capacityMutation(this.hasVainas, newHabProb),
            hasInmortalGen:capacityMutation(this.hasInmortalGen, newHabProb),
            hasMortalGen:capacityMutation(this.hasMortalGen, newHabProb),
            hasGigantoGen:capacityMutation(this.hasGigantoGen, newHabProb),
            hasMiniGen:capacityMutation(this.hasMiniGen, newHabProb),
            hasSporeGen:capacityMutation(this.hasSporeGen, newHabProb),
            hasFertilizerGen:capacityMutation(this.hasFertilizerGen, newHabProb),
            hasStoreGen:capacityMutation(this.hasStoreGen, newHabProb),
            hasComplexGen:capacityMutation(this.hasComplexGen, newHabProb),
            hasFrugalGen:capacityMutation(this.hasFrugalGen, newHabProb),
            hasInestableGen:capacityMutation(this.hasInestableGen, newHabProb),
            hasStableGen:capacityMutation(this.hasStableGen, newHabProb),
            hasInShadowsGrow:capacityMutation(this.hasInShadowsGrow, newHabProb),
        }
    }

    /**
     * generate the new plant position near to the current position
     * @param tileID current position
     * @returns new plant position
     */
    private determinatePropagationDirection(tileID:string):string{
        let xMov:number= probCalc(.5)? -1: 1;
        let yMov:number= probCalc(.5)? -1: 1;
        if(probCalc(.5)){
            xMov= probCalc(.5)? 0: xMov;
        }else{
            yMov= probCalc(.5)? 0: yMov;
        }
        //mutation
        if(this.hasSporeGen){
            if(probCalc(.5)){
                xMov*=2;
                yMov*=3;
            }else{
                xMov*=3;
                yMov*=2;
            }
        }
        let x = parseInt(tileID.substring(0, tileID.indexOf('-')));
        let y = parseInt(tileID.substring(tileID.indexOf('-')+1));
        return (x+xMov)+'-'+(y+yMov);
    }

    get currentSize():number{
        return this.height;
    }

}