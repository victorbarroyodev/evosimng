import { probCalc } from "../utils/math.utils";

export class PlantFruit{
    tileID:string;
    isFresh = true;
    nutritionValue: number;

    constructor(tileId:string ,nutritionValue:number){
        this.tileID= tileId;
        this.nutritionValue = nutritionValue;
    }

    tick(fertLvl:number):number{
        let degradateprob = .01 - (fertLvl/100000);
        if(probCalc(degradateprob)){
            if(this.isFresh){
                this.isFresh= !this.isFresh;
            }else{
                return this.nutritionValue/50;
            }
        }
       return 0;
    }
}