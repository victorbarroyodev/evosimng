import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Tile } from 'src/app/shared/models/tile.model';
import { PlantSeed } from 'src/app/shared/models/plantSeed.model';
import { PlantFruit } from '../../models/plantFruit.model';

@Injectable({
  providedIn: 'root'
})
export class ComunicationService {
  private clickedTileSubject: Subject<Tile> = new Subject<Tile>();
  private newSeedSubject: Subject<PlantSeed> = new Subject<PlantSeed>();
  private newFruitSubject: Subject<PlantFruit> = new Subject<PlantFruit>();
  private giveFertSubject: Subject<{pos:string, val:number}> = new Subject<{pos:string, val:number}>();

  /**
   * create a new event with the tile info
   * @param tile the tile info
   */
  nextclickedTileEvent(tile:Tile){
    this.clickedTileSubject.next(tile);
  }

  /**
   * Observable that emit the events with the tile info
   */
  get $clickedTileObservable(): Observable<Tile>{
    return this.clickedTileSubject.asObservable();
  }

  /**
   * create a new event with the seed info
   * @param seed the seed info
   */
  nextnewSeedEvent(seed:PlantSeed){
    this.newSeedSubject.next(seed);
  }

  /**
   * Observable that emit the events with the seed info
   */
  get $newSeedobservable():Observable<PlantSeed>{
    return this.newSeedSubject.asObservable();
  }

  nextnewFruitEvent(fruit:PlantFruit){
    this.newFruitSubject.next(fruit);
  }

  get $newfruitObservable():Observable<PlantFruit>{
    return this.newFruitSubject.asObservable();
  }

  nextGiveFertEvent(pos:string, val:number){
    this.giveFertSubject.next({pos,val});
  }

  get $giveFertObservable():Observable<{pos:string, val:number}>{
    return this.giveFertSubject.asObservable();
  }

}
