/**
 * Returns randomly if a event occurs with n prob
 * @param n probability of true
 * @returns true with (n*100)% of prob/ false with (100-(n*100))% of prob
 */
export const probCalc = (n:number):boolean => {
    return !!n && Math.random() <= n;
};

export const qualityMutation = (n:number, mutationPower:number = .05):number => {
    if (probCalc(.5)){
        return n* (1+mutationPower);
    }else{
        return n* (1-mutationPower);
    }
}

export const capacityMutation = (b:boolean, mutationProb:number=.001):boolean => {
    if(probCalc(1-mutationProb)){
        return b;
    }else{
        return !b;
    }
}